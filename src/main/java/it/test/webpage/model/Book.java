package it.test.webpage.model;

public class Book {

    private Long id;
    private String author;
    private Integer pageNum;

    public Book() {}

    public Book(Long id, String author, Integer pageNum) {
        this.id = id;
        this.author = author;
        this.pageNum = pageNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
}
