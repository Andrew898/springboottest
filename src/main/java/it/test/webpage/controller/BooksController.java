package it.test.webpage.controller;

import it.test.webpage.model.Book;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Controller
@RequestMapping("/books")
public class BooksController {

    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = "application/json")
    public ResponseEntity getBook(@PathVariable(value = "id") Long id) {
        if(id.equals(2L)) {
            return ResponseEntity.badRequest().body(new Exception("error"));
        }
        return ResponseEntity.ok().body(new Book(id, "Test", 2));
    }
}
